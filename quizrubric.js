(function ($) {
  Drupal.behaviors.quizrubricUpdateScore = {
    attach: function (context, settings) {
      $('.quiz-report-score-rubric .form-type-radio input', context).click(function (fieldset) {
        // When any radio is clicked, add up the score over all radios.
        var newScore = 0;
        var fieldset = $(this).parents('fieldset.quiz-report-score-rubric');
        $('input.form-radio', fieldset).each(
          function(index, radio) {
            if (radio.checked == true) {
              newScore += parseInt(radio.value);
            }
          }
        );
        
        // Find the score placeholder, update the weighted score value.
        var nid = parseInt(fieldset.attr('qnid'));
        var scoreElement = $('.rubric-score-placeholder.qnid-' + nid, this.form);
        var scoreWeight = parseFloat(scoreElement.attr('scoreweight'));
        newScore = Math.round(newScore * scoreWeight);
        scoreElement.html(newScore);
      });
    }
  };
})(jQuery);
