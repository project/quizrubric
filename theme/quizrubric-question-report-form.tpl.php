<?php

/**
 * @file
 * Themes the quiz rubric question report. This is a copy of the section of 
 * quiz-report-form.tpl.php responsible for themeing individual questions. It 
 * should be updated when quiz-report-form.tpl.php is updated (which is an 
 * unfortunate side effect of using custom templates in modules).
 *
 * Available variables:
 * $sub_form - FAPI array
 */
$c_class = ($sub_form['#is_evaluated']) ? ($sub_form['#is_correct']) ? 'q-correct' : 'q-wrong' : 'q-waiting'; ?>
<div class="dt">
  <div class="quiz-report-score-container <?php print $c_class?>">
  	<span>
    <?php print t('Score')?>
      <?php print drupal_render($sub_form['score'])?>
      <?php print t('of') .' '. $sub_form['max_score']['#value']?>
      <?php if ($sub_form['#is_skipped']): ?>
        <br><em><span class="quiz-report-skipped">
        <?php print t('(skipped)') ?>
        </span></em>
      <?php endif; ?>
      <?php if ($sub_form['#is_doubtful']): ?>
        <br><em><span class="quiz-report-doubtful">
        <?php print t('(Doubtful)') ?>
        </span></em>
      <?php endif; ?>
    </span>
  </div>
  <p class="quiz-report-question"><strong><?php print t('Question')?>: </strong></p>
  <?php print drupal_render($sub_form['question']);?>
</div>
<?php if (!isset($sub_form['response']['#no-response'])): ?>
<div class="dd">
  <p><strong><?php print t('Response')?>: </strong></p>
  <?php print drupal_render($sub_form['response']); ?>
</div>
<?php endif; ?>

<?php if (isset($sub_form['rubric'])): ?>
<div class="dd">
  <?php print drupal_render($sub_form['rubric']); ?>
</div>
<?php endif; ?>

<div class="dd">
  <?php print drupal_render($sub_form['answer_feedback']); ?>
</div>
