<?php


/**
 * The main classes for the rubric-enabled quiz question types.
 *
 * @file
 * Modified classes for some quiz question types to allow scoring via a rubric.
 */

/**
 * Extends QuizfileuploadQuestion to add rubric support.
 */
class QuizfileuploadQuestionRubric extends QuizfileuploadQuestion {
  /**
   * Implementation of delete
   *
   * @see QuizQuestion#delete()
   */
  public function delete($only_this_version = FALSE) {
    if ($only_this_version) {
      db_delete('quizrubric_scores')
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
    else {
      db_delete('quizrubric_scores')
        ->condition('question_nid', $this->node->nid)
        ->execute();
    }
    parent::delete($only_this_version);
  }
  
  
  /**
   * Implementation of getMaximumScore
   */
  public function getMaximumScore() {
    $node = $this->node;
    if (!isset($node->quizfileupload_scoring)) {
      $node = node_load($this->node->nid);
    }
    
    if ($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2) {
      return count($node->field_quizrubric[$node->language]) * 5;
    }
    else {
      return parent::getMaximumScore();
    }
  }
}


/**
 * Extends QuizfileuploadResponse to add rubric support.
 */
class QuizfileuploadResponseRubric extends QuizfileuploadResponse {
  /**
   * Constructor
   */
  public function __construct($result_id, stdClass $question_node, $tries = NULL) {
    parent::__construct($result_id, $question_node, $tries);
  }
  
  
  /**
   * Overridden version that always calculates max score from number of 
   * criteria in rubric if rubric scoring is selected. Note that once a 
   * question has been marked it is no longer possible to change the criteria.
   *
   * @param $weight_adjusted
   *  If the returned max score shall be adjusted according to the max_score the question has in a quiz
   * @return
   *  Max score(int)
   */
  public function getMaxScore($weight_adjusted = TRUE) {
    $node = node_load($this->question->nid);
    
    if ($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2) {
      $this->question->max_score = count($node->field_quizrubric[$node->language]) * 5;
      
      if (isset($this->question->score_weight) && $weight_adjusted) {
        return round($this->question->max_score * $this->question->score_weight);
      }
      
      return $this->question->max_score;
    }
    else {
      return parent::getMaxScore($weight_adjusted);
    }
  }

  
  /**
   * Implementation of delete
   *
   * @see QuizQuestionResponse#delete()
   */
  public function delete() {
    db_delete('quizrubric_scores')
      ->condition('question_nid', $this->question->nid)
      ->condition('question_vid', $this->question->vid)
      ->condition('result_id', $this->rid)
      ->execute();
    
    parent::delete();
  }
  
  
  /**
   * Creates the report form for the admin pages, and for when a user gets feedback after answering questions.
   *
   * The report is a form to allow editing scores and the likes while viewing the report form
   *
   * @param $showpoints
   * @param $showfeedback
   * @param $allow_scoring
   * @return $form
   *  Drupal form array
   */
  public function getReportForm($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    $node = node_load($this->question->nid);
    if (isset($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value']) && $node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2) {
      drupal_add_js(drupal_get_path('module', 'quizrubric'). '/quizrubric.js');
      
      $orig_form = parent::getReportForm($showpoints, $showfeedback, $allow_scoring);
      $form = array();
      foreach ($orig_form as $key => $element) {
        $form[$key] = $element;
        
        // Add the rubric after the score element.
        // (note that the form is usually rendered by a tpl file, so the 
        // ordering isn't really that important, but just in case the tpl 
        // file is discarded (which it hopefully will be) we preserve a 
        // sensible ordering here.)
        if ($key == 'score') {
          $existing_values = db_query(
            "SELECT criteria, score FROM {quizrubric_scores} 
              WHERE result_id = :result_id AND question_nid = :question_nid AND question_vid = :question_vid",
            array(
              ':result_id' => $this->question->answers[0]['result_id'],
              ':question_nid' => $this->question->nid,
              ':question_vid' => $this->question->vid
            ))
            ->fetchAllKeyed();
          
          // Only add the fieldset if we've got something to display.
          if (quiz_access_to_score() && $allow_scoring || !empty($existing_values)) {
            $form['rubric'] = array(
              '#type' => 'fieldset',
              '#tree' => TRUE,
              '#title' => t('Rubric scoring'),
              '#collapsible' => FALSE,
              '#attributes' => array(
                'class' => array('quiz-report-score-rubric'),
                'qnid' => $node->nid
              ),
            );
          }
          
          if (quiz_access_to_score() && $allow_scoring) {
            foreach ($node->field_quizrubric[$node->language] as $delta => $value) {
              $options = array(0 => '0', 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5');
              $form['rubric'][$delta] = array(
                '#type' => 'radios',
                '#title' => $value['value'],
                '#default_value' => empty($existing_values) ? 0 : $existing_values[$delta],
                '#options' => $options,
              );
            }
          }
          else if (!empty($existing_values)) {
            $form['rubric']['results'] = array(
              '#theme' => 'table',
              '#header' => array(
                t("Criteria"),
                t("Score"),
              ),
              '#rows' => array(),
              '#attributes' => array(
                'class' => array('quiz-report-score-rubric-results'),
              ),
            );
            foreach ($node->field_quizrubric[$node->language] as $delta => $value) {
              $form['rubric']['results']['#rows'][] = array(
                'data' => array(
                  array(
                    'data' => $value['value'],
                    'class' => 'quiz-report-rubric-criteria',
                  ),
                  array(
                    'data' => $existing_values[$delta],
                    'class' => 'quiz-report-rubric-criteria-score',
                  ),
                ),
                'no_striping' => TRUE,
              );
            }
          }
        }
      }
      return $form;
    }
    
    return parent::getReportForm($showpoints, $showfeedback, $allow_scoring);
  }
  
  
  public function getReportFormScore($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    $node = node_load($this->question->nid);
    // If rubric scoring enabled.
    if (quiz_access_to_score() && $allow_scoring && ($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2)) {
      $score = ($this->isEvaluated()) ? $this->getScore() : '?';
      return array(
        '#markup' => '<span class="rubric-score-placeholder qnid-'. $node->nid. '" scoreweight='. $this->question->score_weight. '">'. $score. '</span>',
      );
    }
    else { 
      return parent::getReportFormScore($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE);
    }
  }
  

  /**
   * Implementation of getReportFormSubmit
   *
   * @see QuizQuestionResponse#getReportFormSubmit($showfeedback, $showpoints, $allow_scoring)
   */
  public function getReportFormSubmit($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    if ($allow_scoring && quiz_access_to_score()) {
      $node = node_load($this->question->nid);
      // If rubric enabled then use rubric submit form quizrubric_quizfileupload_report_submit.
      if (isset($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value']) && $node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2) {
        return 'quizrubric_quizfileupload_report_submit';
      }
    }
    return parent::getReportFormSubmit($showfeedback, $showpoints, $allow_scoring);
  }
  
  
  /**
   * Implementation of getReportFormValidate
   *
   * @see QuizQuestionResponse#getReportFormValidate($showfeedback, $showpoints, $allow_scoring)
   */
  public function getReportFormValidate($showfeedback = TRUE, $showpoints = TRUE, $allow_scoring = FALSE) {
    // If rubric enabled then don't need validate.
    $node = node_load($this->question->nid);
    if (isset($node->quizfileupload_scoring[LANGUAGE_NONE][0]['value']) && $node->quizfileupload_scoring[LANGUAGE_NONE][0]['value'] == 2) {
      return FALSE;
    }
    return parent::getReportFormValidate($showfeedback, $showpoints, $allow_scoring);
  }
  

  /**
   * Implementation of getReportFormTheme
   */
  public function getReportFormTheme($showfeedback = TRUE, $showpoints = TRUE) {
    return 'quizrubric-question-report-form';
  }
}
